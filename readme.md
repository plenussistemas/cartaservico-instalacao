# Carta de Serviços - Instalação #

Este repositório, aborda o passo a passo para a instalação de um novo cliente de Carta de Serviços.

#### Pré-requisitos

* PHP 7.2.5;
* Postgres 11;
* Instalar localmente o Legislarr;

#### Pré-requisitos para testes locais
* Todos os pré-requisitos anteriores;
* Instalar localmente o Carta de Serviços;

### Passo a passo para instalar o Carta de Serviços (Produção)
##### 1. Instalação do Legislarr
* Faça um fork do projeto oficial do [Legislarr](https://bitbucket.org/plenussistemas/legislarr-admin/src/master/);
* No seu repositório do Legislarr, o qual você acabou de fazer um fork. Faça um `git clone` do mesmo para a sua máquina local
* Após realizado o clone para a sua máquina local, entre na pasta do projeto:  
`cd legislarr-admin`
* Na raiz do projeto, para instalar todas as dependências, execute o seguinte comando:  
`composer install`
* Ainda na raiz do projeto. Crie o arquivo .env, copiando do arquivo exemplo. Você pode usar o seguinte comando:  
`cp .env.example .env`


##### 2. Configurações no Google SQL
* Verifique se seu [IP](https://www.meuip.com.br/) está habilitado para se conectar ao [Banco de Dados](https://console.cloud.google.com/sql/instances/legislarr-admin/connections?project=intricate-gamma-262511) em produção
* O seu IP tem que estar listado dentro de Conectividade -> IP Público -> Redes autorizadas
* Você pode adicionar o seu [IP](https://www.meuip.com.br/) clicando em Adicionar Rede e depois em Salvar, aguarde 1 ou 2 minutos, e seu IP já estará habilitado
* Com o acesso liberado, conecte-se ao banco em produção, e crie o schema do novo cliente. Supondo que o novo cliente seja a Câmara de Maringá,
execute a seguinte query no PostgreSQL: `CREATE SCHEMA IF NOT EXISTS cm_maringa`

##### 3. Cadastrando os dados do novo Cliente em produção
* Acesse o Legislarr no cliente demo de [Maringá](http://admin.legislarr.com.br/login/camaramaringa.com.br);
* No menu Clientes, faça o cadastro dos dados do novo cliente;
* **ATENÇÃO**, o campo SCHEMA, tem que 
ter o mesmo nome do SCHEMA que você acabou de criar com a query do *item 2*. O campo domínio, tem que ser o domínio exato
do novo cliente. Por exemplo, para um novo cliente de Carta de Serviços da Câmara de Maringá, o preenchimento do campo ficaria assim:  
`cartadeservicos.camaramaringa.com.br`
 
##### 4. Preparando o projeto para executar comandos em produção
* Na sua máquina local, na raiz do projeto do Legislarr, acesse o arquivo .env que você criou anteriormente;
* Altere os valores das seguintes variáveis de ambientes   
`DB_HOST=[IP_DO_BANCO_EM_PRODUCAO]` - Que pode ser visualizado [aqui](https://console.cloud.google.com/sql/instances/legislarr-admin/databases?project=intricate-gamma-262511)  
`DB_PORT=5432`  
`DB_DATABASE=legislarr_admin`  
`DB_USERNAME=postgres`  
`DB_PASSWORD=[SENHA_DO_BANCO_EM_PRODUCAO]`  
* Sempre que alterar o arquivo .env, execute o seguinte comando na raiz do projeto:  
`php artisan config:clear`
   
##### 5. Executando os comandos Artisan
* **ATENÇÃO** com as configurações do *item 4*, todos os comandos a seguir, serão executados em **Produção**
* Com muito cuidado para não omitir nenhuma flag, execute o seguinte comando:  
`php artisan migrate --schema=[NOME_DO_SCHEMA] --seedInitial`  
para rodar os migrations em Produção;
* Note que em `--schema=[NOME_DO_SCHEMA]` você irá preencher com o nome do SCHEMA do novo cliente que você criou anteriormente,
o seu comando vai ser parecido com o seguinte comando de exemplo:  
`php artisan migrate --schema=cm_maringa --seedInitial`
* É normal o comando de rodar os migrations em produção demorar um pouco, ou não emitir resposta alguma, fique calmo ele está rodando,
aguarde ele terminar; 

##### 6. Configurando Módulos
* O comando de rodar os migrations do *item 5*, criou todas as tabelas com seus respectivos seeders. Isso quer dizer que esse novo cliente possui
todos os módulos instalados;
* Conecte-se ao banco em produção, selecione o schema criado do novo cliente, e na tabela `modulo`, visualize os módulos
que o cliente possui. **ATENÇÃO** Nunca remova os módulos `Usuário e Arquivo`
* Para um cliente de Carta de Serviços, ele precisará dos seguintes módulos, ou seja, não os remova:    
`Arquivo`  
`Usuário`  
`Carta de Serviço`  
`Agendamento` - Se o cliente possuir o módulo de Agendamento de Serviços  

##### 7. Configurando DNS
* Para o site de Carta de Serviços do cliente funcionar. Será necessário que o sudomínio de Carta de Serviços do cliente, 
aponte para o IP (35.237.118.15) do servidor que o Legislarr está alocado;
* Por exemplo, se estivermos criando um subdomínio para a Câmara de Maringá, teríamos que fazer o sudomínio:  
`cartadeservicos.camaramaringa.com.br`  
apontar para o IP 35.237.118.15

##### 8. Configurando Virtual Host
* Acesse o servidor via SSH; 
* É recomendado que você use o serviço do Google, de SSH via navegador;
* Nesse [link](https://console.cloud.google.com/compute/instances?project=intricate-gamma-262511&instancessize=50)
na instância que está com o check verde, chamada de `legislar`, clique em SSH;
* Aguarde abrir uma nova janela com a acesso ao servidor;
* Abra o arquivo de Virtual Host do Carta de Serviços com o seguinte comando:  
`sudo vim /etc/apache2/sites-available/cartaservico.com.br.conf`
* Esse arquivo já possui várias configurações de outros clientes, nesse caso, é só você copiar a configuração já em uso de um cliente,
e no final do arquivo colar essa configuração, mudando os dados necessários, por exemplo:
![VirtualHost](./img/virtualhost.png)
* Você só precisa mudar o `ServerName` e o `ServerAlias`
* Feito esta edição, é necessário reiniciar o Apache, com o seguinte comando:  
`sudo systemctl restart apache2`

##### 9. Acessando o novo Cliente
* Se tudo deu certo, agora você já pode acessar o novo cliente tanto o Admin quanto o Site 
* URL Admin:  
`http://admin.portyx.com.br/login/[DOMINIO_DO_CLIENTE]`  
`http://admin.portyx.com.br/login/cartadeservicos.camaramaringa.com.br` - Por exemplo
* URL Site:  
`http://cartadeservicos.[SITE_DO_CLIENTE]`  
`http://cartadeservicos.camaramaringa.com.br` - Por exemplo  